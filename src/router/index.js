/* eslint-disable */
import Vue from "vue";
import VueRouter from "vue-router";

//import Home from '../pages/home/Home.vue';

// for auction
import CreateAuction from '../pages/auction/AuctionCreate.vue';

//User Authentication
import Signin from '../pages/auth/Signin.vue';
import Signup from '../pages/auth/Signup.vue';

//for bidder
import BiddingList from '../pages/bidder/BiddingList.vue';
import BidderDetail from '../pages/bidder/BidderDetail.vue';
import BidderEdit from '../pages/bidder/BidderEdit.vue';

//for product
import Products from '../pages/products/Products.vue';
import ProductDetails from '../pages/products/ProductDetails.vue';
import ProductDetailsAdmin from '../pages/products/ProductDetailsAdmin.vue';
import ProductCreate from '../pages/products/ProductCreate.vue';
import ProductEdit from '../pages/products/ProductEdit.vue';



Vue.use(VueRouter);

export const routes = [
  {
      path: '/', 
      redirect: '/products'
  },

  // for auction
  {
    path: '/createauction/:id', 
    component: CreateAuction
},
  
  //User Authentication

  {
      path: '/signin',
      component: Signin
  },
  {
      path: '/signup',
      component: Signup
  },

  //for bidder
  {
      path: '/biddings',
      component: BiddingList
  },
  {
    path: '/bidding/:id',
    component: BidderDetail
  },
  {
    path: '/bidding/:id/edit',
    component: BidderEdit
  },

  //for product
  {
      path: '/products',
      component: Products
  },
  {
    path: '/products/:id',
    component: ProductDetails
  },
  {
    path: '/productsadmin/:id',
    component: ProductDetailsAdmin
  },
  {
    path: '/createproduct',
    component: ProductCreate
  },
  {
    path: '/products/:id/edit',
    component: ProductEdit
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
