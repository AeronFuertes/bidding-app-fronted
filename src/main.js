import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { createProvider } from "./vue-apollo";
import moment from "moment";
import Notifications from "vue-notification";

// Carousel
import Carousel from "./components/carousel/Carousel.vue";
Vue.config.productionTip = false;
Vue.prototype.moment = moment;
Vue.component("carousel", Carousel);
Vue.use(Notifications);

new Vue({
  router,
  store,
  apolloProvider: createProvider(),
  render: (h) => h(App),
}).$mount("#app");
